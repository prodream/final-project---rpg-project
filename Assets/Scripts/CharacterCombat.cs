﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterStats))]
public class CharacterCombat : MonoBehaviour
{
    public float attackSpeed = 1f;
    private float attackCooldown = 0f;
    const float combatCooldown = 5f;
    float lastAttackTime;

    public float attackDelay = .6f;

    CharacterStats myStats;
    CharacterStats opponentStats;

    public bool InCombat { get; private set; }
    public event System.Action OnAttack;

    void Start()
    {
        myStats = GetComponent<CharacterStats>(); // Check damage hoặc armor dựa trên character stats
    }

    void Update()
    {
        attackCooldown -= Time.deltaTime;
        if(Time.time - lastAttackTime > combatCooldown)
        {
            InCombat = false;
        }
    }

    public void Attack(CharacterStats targetStat)
    {
        if(attackCooldown < 0f)
        {
            // Test gây sát thương lên đối tượng dựa vào stats damage
            //targetStat.TakeDamage(myStats.damage.GetValue());
            opponentStats = targetStat;
            if(OnAttack != null)
            {
                OnAttack();
            }

            // Cách mới là trigger dùng coroutine
            //StartCoroutine(DoDamage(targetStat, attackDelay));
            attackCooldown = 1f / attackSpeed;
            InCombat = true;
            lastAttackTime = Time.time;
        }
        
    }

        // Test sang method attack_animationEvent
    //IEnumerator DoDamage(CharacterStats stats, float delay)
    //{
    //    // Delay
    //    yield return new WaitForSeconds(delay);
    //    //// Gây sát thương
    //    //stats.TakeDamage(myStats.damage.GetValue());
    //    //if(stats.currentHealth <= 0)
    //    //{
    //    //    InCombat = false;
    //    //}
    //}

    public void attack_AnimationEvent()
    {
        // Gây sát thương
        opponentStats.TakeDamage(myStats.damage.GetValue());
        if (opponentStats.currentHealth <= 0)
        {
            InCombat = false;
        }
    }
}
