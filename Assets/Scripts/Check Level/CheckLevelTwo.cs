﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckLevelTwo : MonoBehaviour
{
    public int LoadLevel = 1;
    float lastTimeActive;
    float activeShow = 5f;
    PlayerManager player;

    private void Start()
    {
        player = PlayerManager.instance;
    }

    void Update()
    {
        if (Time.time - lastTimeActive > activeShow)
        {
            Master.instance.masterPanel.SetActive(false);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (player.EnemyKilled == 5)
            {
                Master.instance.guildText.text = "Congratulation, you completed the game";
                Master.instance.masterPanel.SetActive(true);
                lastTimeActive = Time.time;
            }
            else
            {
                Master.instance.guildText.text = "Your enemy kill is: " + player.EnemyKilled + "/5, Must kill all";
                Master.instance.masterPanel.SetActive(true);
                lastTimeActive = Time.time;
            }
        }
    }


}
