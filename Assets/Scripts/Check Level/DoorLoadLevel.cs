﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorLoadLevel : MonoBehaviour
{
    public int LoadLevel = 1;
    float lastTimeActive;
    float activeShow = 5f;
    PlayerManager player;

    private void Start()
    {
        player = PlayerManager.instance;
    }

    void Update()
    {
       if(Time.time - lastTimeActive > activeShow)
        {
            Master.instance.masterPanel.SetActive(false);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (player.EnemyKilled == 3)
            {
                Master.instance.guildText.text = "Stay and Press E to continue";
                Master.instance.masterPanel.SetActive(true);
                lastTimeActive = Time.time;
            }
            else
            {
                Master.instance.guildText.text = "Your enemy kill is: " + player.EnemyKilled + "/3, Must kill all";
                Master.instance.masterPanel.SetActive(true);
                lastTimeActive = Time.time;
            }
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (player.EnemyKilled == 3)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    SceneManager.LoadScene(LoadLevel);
                }
            }
            
        }
    }

    
}
