﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour
{
    #region singleton
    public static EquipmentManager instance;
    void Awake()
    {
        instance = this;
    }
    #endregion

    public Equipment[] defaultItems;
    public SkinnedMeshRenderer targetMesh;
    Equipment[] currentEquipment; // Đây là array các item đã được equip-trang bị
    SkinnedMeshRenderer[] currentMeshes;

    // Đây là 1 hàm call back khi 1 item đang trang bị/không trang bị
    public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
    public OnEquipmentChanged onEquipmentChanged;

    Inventory inventory;    // Đây là reference tới inventory

    public List<GameObject> itemDrop;

    protected virtual void Start()
    {
        inventory = Inventory.instance;
        int numSlot = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        currentEquipment = new Equipment[numSlot];
        currentMeshes = new SkinnedMeshRenderer[numSlot];
        EquipDefaultItems();
    }

    public void Equip(Equipment newItem)
    {
        // Tìm ra slot nào phù hợp với item trang bị
        int slotIndex = (int) newItem.equipSlot;           
        Equipment oldItem = UnEquip(slotIndex);

        // Đoạn code phía trên comment lại vì trước khi equip thì đã có unequip nên ko cần để add back về inventory
        if (onEquipmentChanged != null)
        {
            onEquipmentChanged.Invoke(newItem, oldItem);
        }
        SetEquipmentBlendShapes(newItem, 100);

        // Dòng này để insert item vào các slot
        currentEquipment[slotIndex] = newItem;
        SkinnedMeshRenderer newMesh = Instantiate<SkinnedMeshRenderer>(newItem.mesh);
        newMesh.transform.parent = targetMesh.transform;

        newMesh.bones = targetMesh.bones;
        newMesh.rootBone = targetMesh.rootBone;
        currentMeshes[slotIndex] = newMesh;
    }

        // Chuyển dạng void sang Equipment
    public Equipment UnEquip(int slotIndex)
    {
        // Đoạn code này chạy chỉ khi có item đã được trang bị
        if (currentEquipment[slotIndex] != null)
        {
            if(currentMeshes[slotIndex] != null)
            {
                Destroy(currentMeshes[slotIndex].gameObject);
            }
            Equipment oldItem = currentEquipment[slotIndex];
            SetEquipmentBlendShapes(oldItem, 0);
            inventory.Add(oldItem);
            currentEquipment[slotIndex] = null;
            if (onEquipmentChanged != null)
            {
                onEquipmentChanged.Invoke(null, oldItem);
            }
            return oldItem;
        }
        return null;
    }

    public void UnEquipAll()
    {
        for (int i = 0; i < currentEquipment.Length; i++)
        {
            UnEquip(i);
        }
        EquipDefaultItems();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            UnEquipAll();
        }
    }

    void SetEquipmentBlendShapes(Equipment item, int weight)
    {
        foreach (EquipmentMeshRegions blendShape in item.coveredMeshRegion)
        {
            targetMesh.SetBlendShapeWeight((int)blendShape, weight);
        }
    }

    public void DropItem(Vector3 position)
    {
        position += new Vector3(0, 0.1f, 0);
        Instantiate(itemDrop[Random.Range(0, itemDrop.Count)], position, Quaternion.identity);
    }

    public void EquipDefaultItems()
    {
        foreach(Equipment item in defaultItems)
        {
            Equip(item);
        }
    }
}
