﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    #region Singleton

    public static PlayerManager instance;
    public bool checkPlayerDied = false;

    void Awake()
    {
        instance = this;
    }

     void Update()
    {
        if (Input.GetKeyDown(KeyCode.T) && checkPlayerDied)
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);           
        }
    }

    #endregion

    public GameObject player;

    public void KillPlayer()
    {
        Master.instance.diedText.text = "You dead, press T to continue";
        Master.instance.diedTextDisplay.SetActive(true);
        Time.timeScale = 0;
        checkPlayerDied = true;
    }

    public int EnemyKilled = 0;
}
