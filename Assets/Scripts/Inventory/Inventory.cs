﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton
    public static Inventory instance;
    private void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found !");
            return;
        }
        instance = this;
    }

    #endregion

        // Cách dưới dùng delegate, tương đồng với cách sử dụng event
    //public delegate void OnItemChanged();
    //public OnItemChanged onItemChangedCallBack;

    public event System.Action onItemChangedCallBack;


    public int space = 20;

    public List<Item> items = new List<Item>();

    public bool Add(Item item)
    {
        if (!item.isDefaultItem)
        {
            if(items.Count >= space)
            {
                Debug.Log("Not have enough space of inventory !");
                return false;
            }
            items.Add(item);
            if(onItemChangedCallBack != null)
            {
                onItemChangedCallBack.Invoke();
            }
            
        }
        return true;
    }

    public void Remove(Item item)
    {
        items.Remove(item);
        if (onItemChangedCallBack != null)
        {
            onItemChangedCallBack.Invoke();
        }
    }
}
