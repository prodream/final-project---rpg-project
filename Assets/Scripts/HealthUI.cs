﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterStats))]
public class HealthUI : MonoBehaviour
{
    public GameObject uiHealth;
    public Transform targetPosition;
    Transform cam;

    Transform ui; // UI đang spawning
    Image healthSlider;  // The green of the heathbar filling based on the health of character;
    float visibleTime = 5;
    float lastMadeVisibleTime;

    void Start()
    {
        cam = Camera.main.transform;
        foreach (Canvas c in FindObjectsOfType<Canvas>())
        {
            if(c.renderMode == RenderMode.WorldSpace)
            {
                ui = Instantiate(uiHealth, c.transform).transform;
                healthSlider = ui.GetChild(0).GetComponent<Image>();
                ui.gameObject.SetActive(false);
                break;
            }
        }
        GetComponent<CharacterStats>().OnHealthChange += OnHealthChange;

        // Nếu dùng instance thì không cần phải dùng getcomponent
        //CharacterStats.instance.OnHealthChange += OnHealthChange;
    }

    void LateUpdate()
    {
        // Đây là chỗ update position for UI
        if(ui != null)
        {
            ui.position = targetPosition.position;
            ui.forward = -cam.forward;
            if(Time.time - lastMadeVisibleTime > visibleTime)
            {
                //Debug.Log(Time.time - lastMadeVisibleTime);
                ui.gameObject.SetActive(false);
            }
        }

    }

    void OnHealthChange(int maxHealth, int currentHealth)
    {
        if(ui != null)
        {
            ui.gameObject.SetActive(true);
            lastMadeVisibleTime = Time.time;

            float healthPercent = (float)currentHealth / maxHealth;
            healthSlider.fillAmount = healthPercent;
            //Debug.Log(healthPercent);
            if (currentHealth <= 0)
            {
                Destroy(ui.gameObject);
            }
        }
        
    }
}
