﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Interactable
{
    // create a dialogue to the NPC
    public string[] dialogues;
    public string names;

    public override void Interact()
    {
        base.Interact();
        Debug.Log("Interact with NPC");
        DialogueSystem.instance.AddNewDialogue(dialogues, names);
    }
}
