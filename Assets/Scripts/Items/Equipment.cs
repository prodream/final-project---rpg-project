﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment", menuName = "Inventory/Equipment")]
public class Equipment : Item
{
    public EquipmentSlot equipSlot; // cac slot de store cho viec trang bi item
    public SkinnedMeshRenderer mesh;
    public EquipmentMeshRegions[] coveredMeshRegion;

    public int armorModifier;   // tăng và giảm giáp
    public int damageModifier;  // tăng và giảm sát thương
    public override void Use()
    {
        base.Use();
        // Equip the item
        EquipmentManager.instance.Equip(this);
        // Remove it from inventory
        RemoveFromInventory();
    }

}

public enum EquipmentSlot
{
    Head, Chest, Legs, Weapons, Shield, Feet 
}

// Tương tác correspond với body blend shape
public enum EquipmentMeshRegions
{
    Legs, Arms, Torso
}
