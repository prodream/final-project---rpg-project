﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour
{
    public static DialogueSystem instance;
    public List<string> dialogueLines = new List<string>();

    public string npcName;
    public GameObject dialoguePanel;
    Button continueButton;
    Text dialogueText, nameText;
    int dialogueIndex;
    void Awake()
    {
        continueButton = dialoguePanel.transform.Find("Continue").GetComponent<Button>();
        dialogueText = dialoguePanel.transform.Find("Text").GetComponent<Text>();
        nameText = dialoguePanel.transform.Find("Name").GetChild(0).GetComponent<Text>();

        continueButton.onClick.AddListener(delegate{ ContinueTheDialogue(); });
        dialoguePanel.SetActive(false);
        if (instance != null)
        {
            Debug.Log("Instance already exist");
            return;
        }
        instance = this;
    }

    public void AddNewDialogue(string[] lines, string npcName)
    {
        dialogueIndex = 0;
        dialogueLines = new List<string>(lines.Length);
        dialogueLines.AddRange(lines);
        this.npcName = npcName;
        Debug.Log(dialogueLines.Count);
        CreateDialogue();
    }

    public void CreateDialogue()
    {
        dialogueText.text = dialogueLines[dialogueIndex];
        dialogueIndex++;
        nameText.text = npcName;
        dialoguePanel.SetActive(true);
    }

    public void ContinueTheDialogue()
    {
        if(dialogueIndex <= dialogueLines.Count -1)
        {
            //dialogueIndex++;
            dialogueText.text = dialogueLines[dialogueIndex];
            dialogueIndex++;
        }
        else
        {
            dialoguePanel.SetActive(false);
        }
    }
}
