﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartDialogue : MonoBehaviour
{
    public static StartDialogue instance;
    public List<string> dialogueLines = new List<string>();

    public GameObject startDialogue;
    Button continueButton;
    Text dialogueText;
    int dialogueIndex;
    void Awake()
    {
        continueButton = startDialogue.transform.Find("Continue").GetComponent<Button>();
        dialogueText = startDialogue.transform.Find("Text").GetComponent<Text>();

        continueButton.onClick.AddListener(delegate { ContinueTheDialogue(); });
        if (instance != null)
        {
            Debug.Log("Instance already exist");
            return;
        }
        instance = this;
    }

    public void CreateTheDialogue()
    {
        dialogueIndex = 0;
        dialogueText.text = dialogueLines[dialogueIndex];
        dialogueIndex++;
        Debug.Log(dialogueLines[dialogueIndex]);
    }

    public void ContinueTheDialogue()
    {
        if (dialogueIndex <= dialogueLines.Count - 1)
        {
            //dialogueIndex++;
            dialogueText.text = dialogueLines[dialogueIndex];
            dialogueIndex++;
        }
        else
        {
            startDialogue.SetActive(false);
        }
    }
}
