﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Master : MonoBehaviour
{
    public static Master instance;
    public GameObject masterPanel;
    public GameObject pauseTextDisplay;
    public GameObject diedTextDisplay;
    public Text guildText, pauseText, diedText;
    
    
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        guildText = masterPanel.transform.Find("GuildText").GetComponent<Text>();
        masterPanel.SetActive(false);
        pauseText = pauseTextDisplay.transform.Find("pauseText").GetComponent<Text>();
        pauseTextDisplay.SetActive(false);
        diedText = diedTextDisplay.transform.Find("DiedText").GetComponent<Text>();
        diedTextDisplay.SetActive(false);

    }

}
