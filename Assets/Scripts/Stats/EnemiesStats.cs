﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesStats : CharacterStats
{
    public override void Die()
    {
        base.Die();

        // Thêm hiệu ứng tan biến khi chết (Animation cho lúc chết)
        EquipmentManager.instance.DropItem(transform.position);
        Destroy(gameObject);
        PlayerManager.instance.EnemyKilled++;
    }

  
}
