﻿using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth { get; private set; }

    public event System.Action<int, int> OnHealthChange;

    public Stat damage;
    public Stat armor;


    //public static CharacterStats instance;
    void Awake()
    {
        currentHealth = maxHealth;
        //instance = this;
    }


    void Update()
    {
        
    }

    public void TakeDamage(int damage)
    {
        damage -= armor.GetValue();
        // Giữ cho damage trong khoảng min là 0 và max tùy định để không cho máu luôn đầy
        damage = Mathf.Clamp(damage, 0, int.MaxValue);
        currentHealth -= damage;
        Debug.Log(transform.name + " take " + damage + " damages.");

        if(OnHealthChange != null)
        {
            OnHealthChange(maxHealth, currentHealth);
        }

        if(currentHealth <= 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        // Chết như thế nào 
        // Hàm này sẽ được kế thừa bởi 2 class Player và Enemies
        Debug.Log(transform.name + " Die.");
    }
}
