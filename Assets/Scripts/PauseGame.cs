﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    Master GameMaster;
    bool checkPause = false;
    private void Start()
    {
        GameMaster = Master.instance;
    }

    void Update()
    {
        Pausing();
    }

    public void Pausing()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!checkPause)
            {
                Time.timeScale = 0;
                checkPause = true;
                Master.instance.pauseText.text = "Press P to UnPause, Press P again to Pause";
                Master.instance.pauseTextDisplay.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                checkPause = false;
                Master.instance.pauseTextDisplay.SetActive(false);
            }
        }
    }
}
